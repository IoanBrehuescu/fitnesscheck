

let bmiCalculator = function (alter, gewicht, grosse) { 
    let bmiAlter = [

        [18, 24, 29, 39],
        [19, 25, 30, 40],
        [20, 26, 31, 41],
        [21, 27, 32, 42],
        [22, 28, 33, 43],
        [23, 29, 34, 44]
    
    ]
    let bmiResult = '';
    let zeile;
   
//Berechne den BMI mit der Formel Gewicht geteilt durch das Quadrat der Größe(in Metern).
    const bmi = gewicht / ((grosse / 100) * (grosse / 100));

    //Bestimme, welche Zeile aus dem bmiAlter-Array basierend auf dem Alter der Person verwendet werden soll, entsprechend dem Index für den passenden Altersbereich.
    if (alter >= 19 && alter <= 24) { zeile = 0; }
    else if (alter >= 25 && alter <= 34) { zeile = 1; }
    else if (alter >= 35 && alter <= 44) { zeile = 2; }
    else if (alter >= 45 && alter <= 54) { zeile = 3; }
    else if (alter >= 55 && alter <= 64) { zeile = 4; }
    else if (alter >= 64) { zeile = 5; };

    //Vergleiche den berechneten BMI mit den Werten aus dem dem Alter entsprechenden Array, um die Gewichtskategorie zu bestimmen.
    if (bmi < bmiAlter[zeile][0]) bmiResult = '<p style="background-color:red">Untergewicht: </p>' + ' ' + '<p style="background-color:black"> Eine Ernährungsberatung kann hilfreich sein, um einen ausgewogenen Ernährungsplan zu erstellen.</p>';
    else if (bmi < bmiAlter[zeile][1]) bmiResult = '<p style="background-color:green">Normalgewicht:</p>' + ' ' + '<p style="background-color:black">Halten Sie an einer ausgewogenen Ernährung fest und achten Sie darauf, eine Vielzahl von Lebensmitteln zu sich zu nehmen, um alle notwendigen Nährstoffe zu erhalten.</p>';
    else if (bmi < bmiAlter[zeile][2]) bmiResult = '<p style="background-color:orange">Übergewicht:</p>' + ' ' + '<p style="background-color:black"> Eine ausgewogene Ernährung mit einer Mischung aus Kohlenhydraten, Proteinen und gesunden Fetten kann nützlich sein.</p>';
    else if (bmi < bmiAlter[zeile][3]) bmiResult = '<p style="background-color:orange">Adipositas:</p>' + ' ' + '<p style="background-color:black">Ein Ernährungsberater kann Ihnen helfen, einen spezifischen Ernährungsplan zu erstellen, um das Gewicht auf gesunde Weise zu reduzieren.</p>';
    else bmiResult = '<p style="background-color:red">Starke Adipositas:</p>' + ' ' + '<p style="background-color:black">Es ist dringend empfohlen, professionelle Ernährungsberatung in Anspruch zu nehmen, um einen gesunden und sicheren Weg zu finden, das Gewicht zu reduzieren und gesundheitliche Risiken zu minimieren.</p>';

    return bmiResult;
}

//Die Funktion akzeptiert zwei Argumente, systolic für den systolischen Blutdruck und diastolic für den diastolischen Blutdruck.
let blutdruckCalculator = function (systolic, diastolic) {
    let bpResult = '';
    if (systolic < 105 && diastolic < 65) bpResult = '<p style="background-color:yellow">Niedrig:</p>' + ' ' + '<p style="background-color:black">Erhöhen Sie den Salzkonsum moderat, aber konsultieren Sie einen Arzt vor jeder größeren Ernährungsänderung. Trinken Sie genug Flüssigkeiten.</p>';
    else if (systolic <= 120 && diastolic <= 80) bpResult = '<p style="background-color:green">Optimal:</p>' + ' ' + '<p style="background-color:black">Halten Sie an einer ausgewogenen Ernährung fest, reich an Obst und Gemüse. Vermeiden Sie übermäßigen Salzkonsum.</p>';
    else if (systolic <= 130 && diastolic <= 85) bpResult = '<p style="background-color:green">Normal:</p>' + ' ' + '<p style="background-color:black">Bleiben Sie bei einer herzgesunden Ernährung. Begrenzen Sie gesättigte Fette und Salz.</p>';
    else if (systolic <= 139 && diastolic <= 89) bpResult = '<p style="background-color:orange">Hochnormal:</p>' + ' ' + '<p style="background-color:black">Reduzieren Sie Salz und Alkohol in Ihrer Ernährung. Erwägen Sie, mehr kaliumreiche Lebensmittel zu essen.</p>';
    else if (systolic <= 159 && diastolic <= 99) bpResult = '<p style="background-color:orange">Hypertonie 1. Grades:</p>' + ' ' + '<p style="background-color:black">Ernährungsumstellungen wie die DASH-Diät können helfen. </p>';
    else if (systolic <= 179 && diastolic <= 109) bpResult = '<p style="background-color:red">Hypertonie 2. Grades:</p>' + ' ' + '<p style="background-color:black">Begrenzen Sie streng den Salzkonsum und vermeiden Sie verarbeitete Lebensmittel. </p>';
    else bpResult = '<p style="background-color:red">Hypertonie 3. Grades:</p>' + ' ' + '<p style="background-color:black">Es ist dringend notwendig, einen Ernährungsplan mit einem Spezialisten zu erstellen. Salz und Alkohol sollten minimiert werden. </p>';
    return bpResult;
}


let koerperfettErgebnis = function (geschlecht, alter, koerperfett) {
    let result = ' ';
    let table, zeile;
    let koerperfettMaenner = [
        [15, 19, 23],
        [17, 20, 24],
        [18, 22, 24],
        [19, 23, 26],
        [21, 23, 27],
        [22, 25, 28],
        [23, 26, 29],
        [24, 27, 30]
    ];

    let koerperfettFrauen = [
        [22, 25, 30],
        [22, 25, 30],
        [23, 26, 31],
        [24, 38, 32],
        [26, 29, 33],
        [27, 31, 34],
        [30, 33, 36],
        [31, 34, 37]

    ];

//checken welche geschlecht gewelt ist
    if (geschlecht == 'maennlich') {
        table = koerperfettMaenner;

    } else if (geschlecht == 'weiblich') {
        table = koerperfettFrauen;

    }
     //Bestimme, welche Zeile aus dem table-Arrais basierend auf dem Alter der Person verwendet werden soll, entsprechend dem Index für den passenden Altersbereich.
    if (alter >= 20 && alter <= 24) { zeile = 0; }
    else if (alter >= 25 && alter <= 29) { zeile = 1; }
    else if (alter >= 30 && alter <= 34) { zeile = 2; }
    else if (alter >= 35 && alter <= 39) { zeile = 3; }
    else if (alter >= 40 && alter <= 44) { zeile = 4; }
    else if (alter >= 45 && alter <= 49) { zeile = 5; }
    else if (alter >= 50 && alter <= 59) { zeile = 6; }
    else if (alter >= 60) { zeile = 7; };
//Nachdem wir das Alter erfahren und es dem entsprechenden Platz zugeordnet haben, können wir basierend auch auf das Geschlecht Empfehlungen geben.
    if (koerperfett >= koerperfettMaenner[0][0] && koerperfett < table[zeile][1]) result = '<p style="background-color:green">Gut:</p>' + ' ' + '<p style="background-color:black">Halten Sie an einer ausgewogenen Ernährung fest, die reich an Vollkornprodukten, Obst und Gemüse ist. </p>';
    else if (koerperfett < table[zeile][2]) result = '<p style="background-color:orange">Mittel:</p>' + ' ' + '<p style="background-color:black">Achten Sie auf Ihre Kalorienzufuhr und versuchen Sie, mehr Protein und Ballaststoffe zu konsumieren. </p>';
    else if (koerperfett >= table[zeile][2]) result = '<p style="background-color:red">Hoch:</p>' + ' ' + '<p style="background-color:black">Es ist wichtig, die Zufuhr von verarbeiteten Lebensmitteln, Zucker und gesättigten Fetten zu reduzieren. Erhöhen Sie den Verzehr von Gemüse, Vollkornprodukten und Proteinen.</p>';

    return result;
}



let clickHandler = function (event) {
    event.preventDefault();

    let inputs, firstname, lastname, email, height, weight, systolic, diastolic, bodyfat, geschlecht, age, error = false;


    //Dupa ce am deja o valoare scrisa daca apas din nou pe casuta valoarea va redeveni goala
    // Wir wählen alle Eingabefelder vom Typ Text und E-Mail aus.
    inputs = document.querySelectorAll('input[type="text"], input[type="email"]');

    // Für jedes Eingabefeld fügen wir einen Event-Listener für das 'focus'-Ereignis hinzu.
    inputs.forEach(input => {
        input.addEventListener('focus', function () {
            this.value = ''; // Wir setzen den Wert des Eingabefelds auf einen leeren String.

        });
    });




    document.querySelector('#vorname').classList.remove('error');
    document.querySelector('#nachname').classList.remove('error');
    document.querySelector('#email').classList.remove('error');
    document.querySelector('#alter').classList.remove('error');
    document.querySelector('#grosse').classList.remove('error');
    document.querySelector('#gewicht').classList.remove('error');
    document.querySelector('#systolic').classList.remove('error');
    document.querySelector('#diastolic').classList.remove('error');
    document.querySelector('#koerperfett').classList.remove('error');



    firstname = document.getElementById('vorname').value;
    lastname = document.getElementById('nachname').value;
    email = document.getElementById('email').value;
    age = document.getElementById('alter').value * 1;
    height = document.getElementById('grosse').value * 1;
    weight = document.getElementById('gewicht').value * 1;
    systolic = document.getElementById('systolic').value * 1;
    diastolic = document.getElementById('diastolic').value * 1;
    bodyfat = document.getElementById('koerperfett').value * 1;


    //Geschlecht Validation

    if (maennlich.checked) {
        geschlecht = 'maennlich';
    } else if (weiblich.checked) {
        geschlecht = 'weiblich';
    } else {
        error = true;
        document.getElementById("keinGeschlecht").innerHTML = '<p style="color:red"> * Geschlecht ist nicht gewählt! </p>'
        let radioButtons = document.querySelectorAll('.form-check-input');
        radioButtons.forEach(function (radio) {
            radio.classList.add('error'); // Fügt die Klasse 'error' jedem Radio-Button hinzu
        });
        radioButtons.forEach(function (radio) {
            radio.addEventListener('click', function () {
                radioButtons.forEach(function (r) {
                    r.classList.remove('error'); // Entfernt die Klasse 'error' von allen Radio-Buttons
                    document.getElementById("keinGeschlecht").innerHTML = '';
                });
            });
        });
    }


    // Vorname, Nachname  Validation (/[^a-zA-Z]/)== Nur Buchstaben!!!
    if (/[^a-zA-Z]/.test(firstname) || !firstname) {
        document.querySelector('#vorname').classList.add('error');
        error = true;
        document.getElementById('vorname').value = 'keine Symbole oder Zahlen eingeben!';

        document.getElementById('vorname').addEventListener('focus', function () {
            if (this.classList.contains('error')) {
                document.querySelector('#vorname').classList.remove('error');
                this.value = '';
            }
        });


    }
    if (/[^a-zA-Z]/.test(lastname) || !lastname) {
        document.querySelector('#nachname').classList.add('error')
        error = true;
        document.getElementById('nachname').value = 'keine Symbole oder Zahlen eingeben!'

        document.getElementById('nachname').addEventListener('focus', function () {
            if (this.classList.contains('error')) {
                document.querySelector('#nachname').classList.remove('error');
                this.value = '';
            }
        });

    }

    //E-mail Validation (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/) - type email validation!!!


    if (!email || !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
        document.querySelector('#email').classList.add('error');
        error = true;
        document.getElementById('email').value = 'E-Mail-Adresse ist ungültig!';
    }

    document.getElementById('email').addEventListener('focus', function () {
        if (this.classList.contains('error')) {
            document.querySelector('#email').classList.remove('error');
            this.value = '';
        }
    });

    //Alter Validation
    if (!age || age < 19 || age > 120 || !Number.isInteger(parseFloat(age))) {
        document.querySelector('#alter').classList.add('error')
        error = true;
        document.getElementById('alter').value = 'Mindestalter 19 Jahre!'

        document.getElementById('alter').addEventListener('focus', function () {
            if (this.classList.contains('error')) {
                document.querySelector('#alter').classList.remove('error');
                this.value = '';
            }
        });

    }

    //Größe Validation
    //https://en.wikipedia.org/wiki/List_of_tallest_people
    if (!height || height < 0 || !isFinite(height) || height > 260 || !Number.isInteger(parseFloat(height))) {
        document.querySelector('#grosse').classList.add('error')
        error = true;
        document.getElementById('grosse').value = 'nur Ganzzahlen zwischen 0 und 260 (cm) eingeben !'
        document.getElementById('grosse').addEventListener('focus', function () {
            if (this.classList.contains('error')) {
                document.querySelector('#grosse').classList.remove('error');
                this.value = '';
            }
        });


    }

    //Gewicht Validation
    //https://en.wikipedia.org/wiki/List_of_heaviest_people
    if (weight < 0 || weight > 610 || !isFinite(weight) || !weight || !Number.isInteger(parseFloat(weight))) {
        document.querySelector('#gewicht').classList.add('error')
        error = true;
        document.getElementById('gewicht').value = 'nur Ganzzahlen zwischen 0 und 610 (kg) eingeben!'

        document.getElementById('gewicht').addEventListener('focus', function () {
            if (this.classList.contains('error')) {
                document.querySelector('#gewicht').classList.remove('error');
                this.value = '';
            }
        });
    }


    //Systolic Validation
    if (!systolic || systolic < 0 || !Number.isInteger(parseFloat(systolic))) {
        document.querySelector('#systolic').classList.add('error')
        error = true;
        document.getElementById('systolic').value = 'nur Ganzzahlen eingeben!'

        document.getElementById('systolic').addEventListener('focus', function () {
            if (this.classList.contains('error')) {
                document.querySelector('#systolic').classList.remove('error');
                this.value = '';
            }
        });

    }


    //Diastolic Validation
    if (!diastolic || diastolic < 0 || !Number.isInteger(parseFloat(diastolic))) {
        document.querySelector('#diastolic').classList.add('error')
        error = true;
        document.getElementById('diastolic').value = 'nur Ganzzahlen eingeben!'

        document.getElementById('diastolic').addEventListener('focus', function () {
            if (this.classList.contains('error')) {
                document.querySelector('#diastolic').classList.remove('error');
                this.value = '';

            }
        });
    }


    //Körperfett Validation
    if (!bodyfat || bodyfat < 15 || !Number.isInteger(parseFloat(bodyfat))) {
        document.querySelector('#koerperfett').classList.add('error')
        error = true;
        document.getElementById('koerperfett').value = 'nur Ganzzahlen - Mindestzahl 15  eingeben!'

        document.getElementById('koerperfett').addEventListener('focus', function () {
            if (this.classList.contains('error')) {
                document.querySelector('#koerperfett').classList.remove('error');
                this.value = '';
            }
        });
    }


    //Alles OK Check!
    if (!error) {
        document.getElementById('result').innerHTML = ' <h5 style="background-color:black">BMI Wert:</h5> ' + ' ' + bmiCalculator(age, weight, height) + '<br>' + '<hr>'
            + '<h5 style="background-color:black">Blutdruckwert:</h5>' + ' ' + blutdruckCalculator(systolic, diastolic) + '<br>' + '<hr>'
            + '<h5 style="background-color:black">Körperfett:</h5>' + ' ' + koerperfettErgebnis(geschlecht, age, bodyfat);

    }
}


document.querySelector('button').onclick = clickHandler;